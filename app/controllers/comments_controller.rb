class CommentsController < ApplicationController

def create
  @post = Post.find(params[:post_id])
  @comment = @post.comments.create(comment_params)
  flash[:notice] = "Comment has been created!"
  redirect_to post_path(@post)
end

def edit
end

def update
  @post = Post.find(params[:post_id])
  if @post.comments.update(post_params)
    @comment = @post.comments.update(comment_params)
    flash[:notice] = "Comment successfully updated!"
    redirect_to post_path(@post)
  else
    render.html {render action: 'edit' }
  end
end

def destroy 
  @post = Post.find(params[:post_id])
  @comment = @post.comments.destroy
  redirect_to post_path(@post)
end

 
private
  def comment_params
    params.require(:comment).permit(:text)
end

end
