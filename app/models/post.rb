class Post < ActiveRecord::Base

  has_many :comments

  attr_accessor :author_name
  belongs_to :author, class_name: "User"
 
  before_save :set_author

  def time_since_created
    Time.current - created_at
  end

  def summary 
    "#{title} - #{truncate(text)}"
  end
 
private
  def set_author
    self.author = User.find_or_create_by(name: author_name)
  end

end
